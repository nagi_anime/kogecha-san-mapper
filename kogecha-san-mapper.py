#!/usr/bin/env python3

# ----------------------------------------------------------------------------
#
#   Kogecha San Mapper
#   Copyright (C) Nagi-Tatschi <https://nagianime.tumblr.com/>
#
#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
#   THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
#   DEALINGS IN THE SOFTWARE.
#
# ----------------------------------------------------------------------------

import sys
import argparse
import orjson, os
import numpy as np
import h5py, functools

def parseArg_Hex(x):
    return(int(x, base=16))
def parseArg(preference):
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("--add", "-a", metavar=("INPUT_COLOR", "OUTPUT_COLOR"),
                       action="store", nargs=2, type=parseArg_Hex,
                       help="add a new pair of color to the database")
    group.add_argument("--info", "-i", choices=["RGB", "HSV"],
                       action="store", nargs="?", type=str, const="RGB",
                       help="show info about the database")
    group.add_argument("--view", "-v", choices=["RGB", "HSV"],
                       action="store", nargs="?", type=str, const="RGB",
                       help="view the database")
    # group.add_argument("--finalize", "-f", metavar="DATABASE", const=preference["database"],
    #                    action="store", nargs="?", type=str,
    #                    help="finalize database and save it to the location specified, if no location are specified, the original database would get overwritten")
    group.add_argument("--database", "-d", metavar="DATABASE",
                       action="store", nargs=1, type=str,
                       help="switch to another database, a new database will be created on the location if no database was found")
    group.add_argument("--merge", "-m", metavar=("DATABASE_TO_MERGE", "DATABASES_TO_MERGE"),
                       action="store", nargs="+", type=str,
                       help="merge other databases into the current database")

    return parser.parse_args()

def readPreference():
    if not os.path.isfile(os.path.join(os.path.dirname(__file__), "preference.json")):
        with open(os.path.join(os.path.dirname(__file__), "preference.json"), "wb") as f:
            f.write(orjson.dumps({"database": os.path.normpath(os.path.join(os.path.dirname(__file__), "resource", "database.hdf5"))
                                  }, option=orjson.OPT_INDENT_2))

    with open(os.path.join(os.path.dirname(__file__), "preference.json"), "rb") as f:
        return orjson.loads(f.read())

def checkDatabase(database):
    if not os.path.isdir(os.path.dirname(database)):
        os.mkdir(os.path.dirname(database))
    if not os.path.isfile(database):
        with h5py.File(database, "w") as f:
            x = np.array([0], dtype=np.ubyte)
            y = np.array([0], dtype=np.ubyte)
            z = np.array([0], dtype=np.ubyte)
            u = np.array([0], dtype=np.ubyte)
            v = np.array([0], dtype=np.ubyte)
            w = np.array([0], dtype=np.ubyte)

            f.create_dataset("x", data=x, chunks=True, maxshape=(16777216,))
            f.create_dataset("y", data=y, chunks=True, maxshape=(16777216,))
            f.create_dataset("z", data=z, chunks=True, maxshape=(16777216,))
            f.create_dataset("u", data=u, chunks=True, maxshape=(16777216,))
            f.create_dataset("v", data=v, chunks=True, maxshape=(16777216,))
            f.create_dataset("w", data=w, chunks=True, maxshape=(16777216,))

def add(preference, colors):
    checkDatabase(preference["database"])

    with h5py.File(preference["database"], "r+") as f:
        x = f.get("x")
        y = f.get("y")
        z = f.get("z")
        u = f.get("u")
        v = f.get("v")
        w = f.get("w")

        wx = np.where(x[:] == ((colors[0] >> 16) & 0xFF))
        wy = np.where(y[:] == ((colors[0] >> 8) & 0xFF))
        wz = np.where(z[:] == ((colors[0]) & 0xFF))
        index = functools.reduce(np.intersect1d, (wx, wy, wz))

        if index.size == 1:
            x[index[0]] = ((colors[0] >> 16) & 0xFF)
            y[index[0]] = ((colors[0] >> 8) & 0xFF)
            z[index[0]] = ((colors[0]) & 0xFF)
            u[index[0]] = ((colors[1] >> 16) & 0xFF)
            v[index[0]] = ((colors[1] >> 8) & 0xFF)
            w[index[0]] = ((colors[1]) & 0xFF)
        elif index.size == 0:
            x.resize((x.size + 1,))
            x[x.size - 1] = ((colors[0] >> 16) & 0xFF)
            y.resize((y.size + 1,))
            y[y.size - 1] = ((colors[0] >> 8) & 0xFF)
            z.resize((z.size + 1,))
            z[z.size - 1] = ((colors[0]) & 0xFF)
            u.resize((u.size + 1,))
            u[u.size - 1] = ((colors[1] >> 16) & 0xFF)
            v.resize((v.size + 1,))
            v[v.size - 1] = ((colors[1] >> 8) & 0xFF)
            w.resize((w.size + 1,))
            w[w.size - 1] = ((colors[1]) & 0xFF)
        else:
            raise IndexError

def viewDatabase_RGBToHSV(r, g, b):
    import matplotlib.colors as mcolors

    rgb = np.stack((r, g, b), axis=1).astype(np.double) / 256.
    hsv = mcolors.rgb_to_hsv(rgb)
    hsv[:, 0] = hsv[:, 0] * 360
    hsv[:, 1] = hsv[:, 1] * 100
    hsv[:, 2] = hsv[:, 2] * 100
    hsv = np.rint(hsv).astype(np.short)

    return hsv[:, 0], hsv[:, 1], hsv[:, 2]

def viewDatabaseInfo(preference, type):
    print("\033[01m[Database]\033[00m " + preference["database"])

    checkDatabase(preference["database"])

    with h5py.File(preference["database"], "r") as f:
        x = f.get("x")
        y = f.get("y")
        z = f.get("z")
        u = f.get("u")
        v = f.get("v")
        w = f.get("w")

        if type == "HSV":
            x, y, z = viewDatabase_RGBToHSV(x, y, z)
            u, v, w = viewDatabase_RGBToHSV(u, v, w)

        for i in range(x.size):
            if type == "RGB":
                print(("#" + str(i).zfill(2)).center(10) + " " +
                      hex(x[i])[2:].zfill(2).upper() +
                      hex(y[i])[2:].zfill(2).upper() +
                      hex(z[i])[2:].zfill(2).upper() +
                      " -> " +
                      hex(u[i])[2:].zfill(2).upper() +
                      hex(v[i])[2:].zfill(2).upper() +
                      hex(w[i])[2:].zfill(2).upper())

            elif type == "HSV":
                print(("#" + str(i).zfill(2)).center(10) + " " +
                      "HSV(" + str(x[i]).rjust(4) + "," +
                      str(y[i]).rjust(4) + "," +
                      str(z[i]).rjust(4) + ")"
                      " -> " +
                      "HSV(" + str(u[i]).rjust(4) + "," +
                      str(v[i]).rjust(4) + "," +
                      str(w[i]).rjust(4) + ")")

def viewDatabase(preference, type):
    import matplotlib.pyplot as plt

    checkDatabase(preference["database"])

    with h5py.File(preference["database"], "r") as f:
        if type == "RGB":
            x = np.copy(f.get("x")).astype(np.short)
            y = np.copy(f.get("y")).astype(np.short)
            z = np.copy(f.get("z")).astype(np.short)
            u = np.copy(f.get("u")).astype(np.short)
            v = np.copy(f.get("v")).astype(np.short)
            w = np.copy(f.get("w")).astype(np.short)

        elif type == "HSV":
            x, y, z = viewDatabase_RGBToHSV(f.get("x"), f.get("y"), f.get("z"))
            u, v, w = viewDatabase_RGBToHSV(f.get("u"), f.get("v"), f.get("w"))

        fig = plt.figure()
        ax = fig.gca(projection='3d')

        if type == "RGB":
            ax.set_title(os.path.split(preference["database"])[1])
            ax.set_xlim3d(0, 255)
            ax.set_xlabel('R (Display P3)')
            ax.set_ylim3d(0, 255)
            ax.set_ylabel('G (Display P3)')
            ax.set_zlim3d(0, 255)
            ax.set_zlabel('B (Display P3)')

        elif type == "HSV":
            ax.set_title(os.path.split(preference["database"])[1])
            ax.set_xlim3d(0, 359)
            ax.set_xlabel('H (Display P3)')
            ax.set_ylim3d(0, 100)
            ax.set_ylabel('S (Display P3)')
            ax.set_zlim3d(0, 100)
            ax.set_zlabel('V (Display P3)')

        ax.quiver(x, y, z, u - x, v - y, w - z, normalize=False)

        plt.show()

# def finalizeDatabase(preference, directory):
#     checkDatabase(preference["database"])

#     with h5py.File(preference["database"], "r") as f:
#         pass

def switchDatabase(preference, directory):
    checkDatabase(directory[0])

    preference["database"] = os.path.normpath(os.path.abspath(directory[0]))
    with open(os.path.join(os.path.dirname(__file__), "preference.json"), "wb") as f:
        f.write(orjson.dumps(preference, option=orjson.OPT_INDENT_2))

def mergeDatabase(preference, m_databases):
    checkDatabase(preference["database"])

    with h5py.File(preference["database"], "r+") as f:
        x = f.get("x")
        y = f.get("y")
        z = f.get("z")
        u = f.get("u")
        v = f.get("v")
        w = f.get("w")

        for m_database in m_databases:
            with h5py.File(m_database) as m_f:
                m_x = m_f.get("x")
                m_y = m_f.get("y")
                m_z = m_f.get("z")
                m_u = m_f.get("u")
                m_v = m_f.get("v")
                m_w = m_f.get("w")

                x.resize((x.size + m_x.size,))
                y.resize((y.size + m_y.size,))
                z.resize((z.size + m_z.size,))
                u.resize((u.size + m_u.size,))
                v.resize((v.size + m_v.size,))
                w.resize((w.size + m_w.size,))

                x[x.size - m_x.size: x.size] = m_x[:]
                y[y.size - m_y.size: y.size] = m_y[:]
                z[z.size - m_z.size: z.size] = m_z[:]
                u[u.size - m_u.size: u.size] = m_u[:]
                v[v.size - m_v.size: v.size] = m_v[:]
                w[w.size - m_w.size: w.size] = m_w[:]

def main():
    preference = readPreference()
    args = parseArg(preference)

    if args.add != None:
        add(preference, args.add)
    elif args.info != None:
        viewDatabaseInfo(preference, args.info)
    elif args.view != None:
        viewDatabase(preference, args.view)
    # elif args.finalize != None:
    #     finalizeDatabase(preference, args.finalize)
    elif args.database != None:
        switchDatabase(preference, args.database)
    elif args.merge != None:
        mergeDatabase(preference, args.merge)
    else:
        raise ValueError

    return 0

if __name__ == "__main__":
    sys.exit(main())
